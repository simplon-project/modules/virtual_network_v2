output "vnet_locations" {
  description = "Les régions Azure où les ressources ont été créées"
  value       = { for k, v in azurerm_virtual_network.main : k => v.location }
}

output "virtual_network_ids" {
  description = "Les IDs des ressources des réseaux virtuels Azure"
  value       = { for k, v in azurerm_virtual_network.main : k => v.id }
}

output "virtual_network_names" {
  description = "Les noms des ressources des réseaux virtuels Azure"
  value       = { for k, v in azurerm_virtual_network.main : k => v.name }
}

output "virtual_network_address_spaces" {
  description = "Les espaces d'adressage des réseaux virtuels Azure"
  value       = { for k, v in azurerm_virtual_network.main : k => v.address_space }
}

output "subnet_ids" {
  description = "Les IDs des sous-réseaux Azure"
  value       = { for name, subnet in azurerm_subnet.main : name => subnet.id }
}

output "subnet_address_prefixes" {
  description = "Les préfixes d'adresse des sous-réseaux Azure"
  value       = { for name, subnet in azurerm_subnet.main : name => subnet.address_prefixes }
}
