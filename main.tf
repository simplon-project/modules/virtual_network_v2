locals {
  tags = {
    project = var.project
    owner   = var.owner
  }
}
locals {
  subnets_flat = flatten([
    for vnet in var.vnets : [
      for subnet in vnet.subnets : {
        vnet_name = vnet.name
        subnet    = subnet
      }
    ]
  ])
}
resource "azurerm_virtual_network" "main" {
  for_each            = { for vnet in var.vnets : vnet.name => vnet }
  name                = each.value.name
  location            = var.vnet_location
  resource_group_name = var.resource_group_name
  address_space       = [each.value.cidr]
  dns_servers         = var.dns_servers_vnet
  tags                = local.tags
}

resource "azurerm_subnet" "main" {
  for_each             = { for subnet in local.subnets_flat : "${subnet.vnet_name}-${subnet.subnet.name}" => subnet }
  name                 = "${each.value.vnet_name}-${each.value.subnet.name}"
  virtual_network_name = azurerm_virtual_network.main[each.value.vnet_name].name
  resource_group_name  = azurerm_virtual_network.main[each.value.vnet_name].resource_group_name
  address_prefixes     = [each.value.subnet.cidr]
}

resource "azurerm_network_security_group" "subnet_nsg" {
  count               = var.create_nsg ? 1 : 0
  name                = var.subnet_nsg_name
  location            = var.vnet_location
  resource_group_name = var.resource_group_name
}

resource "azurerm_subnet_network_security_group_association" "subnet_nsg_association" {
  for_each = var.create_nsg ? azurerm_subnet.main : {}

  subnet_id                 = each.value.id
  network_security_group_id = azurerm_network_security_group.subnet_nsg[0].id
}
