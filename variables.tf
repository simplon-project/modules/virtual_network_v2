variable "resource_group_name" {
  description = "Nom du groupe de ressources"
  type        = string
}
variable "vnet_location" {
  description = "region Azure où les ressources seront créées"
  type        = string
}
variable "project" {
  description = "Nom du projet"
  type        = string
}
variable "owner" {
  description = "Nom du propriétaire du projet"
  type        = string
}

variable "vnets" {
  description = "Liste des VNet à créer, chaque VNet étant un objet avec un nom, un bloc CIDR et une liste de sous-réseaux"
  type = list(object({
    name = string
    cidr = string
    subnets = list(object({
      name = string
      cidr = string
    }))
  }))
  default = [
    {
      name = "vnet1"
      cidr = "10.0.0.0/16"
      subnets = [
        {
          name = "snet1"
          cidr = "10.0.0.0/24"
        },
        {
          name = "snet2"
          cidr = "10.0.1.0/24"
        },
        {
          name = "snet3"
          cidr = "10.0.2.0/26"
        },
        {
          name = "snet4"
          cidr = "10.0.3.0/28"
        }
      ]
    },
    {
      name = "vnet2"
      cidr = "192.168.0.0/16"
      subnets = [
        {
          name = "snet1"
          cidr = "192.168.1.0/24"
        },
        {
          name = "snet2"
          cidr = "192.168.2.0/24"
        }
      ]
    }
  ]
}

variable "dns_servers_vnet" {
  description = "Liste des serveurs DNS à utiliser pour le réseau virtuel"
  type        = list(string)
  default     = []
}
variable "create_nsg" {
  description = "Indique si un NSG doit être créé ou non"
  type        = bool
  default     = true
}
variable "subnet_nsg_name" {
  description = "Nom du groupe de sécurité du sous-réseau"
  type        = string
  default     = "subnet-nsg"
}


