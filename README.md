<!-- BEGIN_TF_DOCS -->
# module terraform : AZURE VIRTUAL NETWORK V2

Ce module Terraform permet de créer permet de créer 1 ou plusieurs Vnets avec à l'interieur de chaque Vnet la possibilité de créer un ou plusieurs subnets.

## Prérequis

Les éléments suivants sont nécessaires pour pouvoir utiliser ce module :

- Avoir un compte [Azure](https://portal.azure.com/)
- Avoir installé [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
- Avoir installé [Azure CLI](https://docs.microsoft.com/fr-fr/cli/azure/install-azure-cli)
- Avoir à minima créer un groupe de ressources ```az group create --name "nom du rg" --location "region"```

| Name | Version |
|------|---------|
| terraform | >= 1.0.0 |
| azurerm | >= 3.108 |

## Utilisation

Exemple de variables à définir dans un fichier .tfvars pour utiliser ce module :

```hcl
vnet_location = "francecentral"
resource_group_name = "rg-network"
vnet_name = "vnet-network"
vnet_cidr = ["10.0.0.0"/16]
subnet_names = ["subnet1", "subnet2"]
```
**Remplacez les valeurs des variables par celles qui correspondent à votre environnement et à vos besoins**

Ensuite Copier-coller le code ci-dessous dans votre fichier Terraform.

```hcl
module "vnet" {
  source              = "git@gitlab.com:simplon-project/modules/azurerm_virtual_network.git"
  location            = var.vnet_location
  resource_group_name = var.resource_group_name
  vnet_name           = var.vnet_name
  vnet_cidr           = var.vnet_cidr
  subnet_names        = var.subnet_names

}
```
## Resources

| Name | Type |
|------|------|
| [azurerm_subnet.main](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet) | resource |
| [azurerm_virtual_network.main](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_network) | resource |

## Variables.tf

| Nom | Description | Type | Par défaut | Requis |
|------|-------------|------|---------|:--------:|
| location | La région Azure dans laquelle toutes les ressources doivent être créées. | `string` | n/a | oui |
| resource\_group\_name | Nom du groupe de ressources où le réseau virtuel sera créé | `string` | n/a | oui |
| subnet\_names | Une liste de noms pour les sous-réseaux à créer | `list(string)` | n/a | oui |
| vnet\_cidr | Le bloc CIDR pour le réseau virtuel | `list(string)` | n/a | oui |
| vnet\_name | Le nom du réseau virtuel | `string` | n/a | oui |

## output.tf

| Nom | Description |
|------|-------------|
| subnet\_address\_prefixes | Une carte associant chaque nom de sous-réseau à son préfixe d'adresse |
| subnet\_ids | Une carte associant chaque nom de sous-réseau à son ID |
| virtual\_network\_address\_space | L'espace d'adressage utilisé par le réseau virtuel |
| virtual\_network\_id | L'ID du réseau virtuel |
| virtual\_network\_name | Le nom du réseau virtuel |

## Auteur

- [Kingston](https://gitlab.com/Kingston-run)

<!-- END_TF_DOCS -->